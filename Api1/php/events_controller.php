<?php
	//funció que a partir d'un rol d'events mostrarà un menú amb les opcions habilitades
	mostraMenuAction( $_GET['destination']);
	
	if (!empty($_GET['action'])){
		switch($_GET['action']){
			case 'create': include_once 'events_template_create.php';	
						 break;
			case 'read': include_once 'events_template_read.php';	
						 break;
			case 'sign': include_once 'events_template_sign.php';	
						 break;
			case 'update': include_once 'events_template_update.php';	
						 break;
			case 'delete': include_once 'events_template_delete.php';	
						 break;
			default: return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
						$_SERVER['SERVER_ADDR'], getBaseURI() ));
						break;
		}
	}else{
		include_once 'events_template_read.php';
	}
?>
