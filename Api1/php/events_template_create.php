<?php
	$error="";
	//si hem fet submit al formulari
	if( !empty( $_POST[ '_send' ])){
		include_once( 'sql_repository2.php' );
		//sanejem entrada
		foreach ($_POST as $k => $v){
			$formValues[$k]= cleanInput($v);			
		}

			InsertNewEvents($formValues);

			return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=event', 
						$_SERVER['SERVER_ADDR'], getBaseURI() ));			
	}else{
		
		//generem el contingut buit pels camps value.
		$formValues = Array ( 
			"alta_nom_event" => "",
			"alta_sortida_event" => "",
			"alta_inici_event" => ""
		);
		
	}
?>

<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
$(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'
	});
  });
  </script>
</head>
<form action=<?php echo sprintf("home_controller.php?destination=event&action=create");?> method="POST">
<p><?php echo $error ?></p>
<div class="input">
	<label for = 'alta_nom_event'>Nom</label>
	<input id="alta_nom_event" name='alta_nom_event' required type="text" maxlength="25" value="<?php echo $formValues['alta_nom_event'];?>"/>
</div>	
<div class="input">
	<label for = 'alta_sortida_event'>Lloc de sortida</label>
	<input id="alta_sortida_event" name='alta_sortida_event' type="text" maxlength="25" value="<?php echo $formValues['alta_sortida_event'];?>"/>
</div>
<div class="input">
	<label for = 'alta_inici_event'>Data d'inici</label>
	<input id="datepicker" name='alta_inici_event' required type="date" placeholder="dd/mm/yyyy" value="<?php echo $formValues['alta_inici_event'];?>"/>
</div>	
<input name="_send" type="submit" value="REGISTRAR">
</form>
</html>
