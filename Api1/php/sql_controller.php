<?php
/* Developer 	:	Ferran De San Martin
 * Date			:	04/12/2015 
 * Content		:	Funcions que interactuen amb php i mysql per funcionament del lloc web.
 */

include_once("sql_repository.php");

//evalua si existeix a la base de dades l'usuari amb el password i retorna TRUE.
function evaluaLogin($userVal,$passVal){
	
	$r = selectAllFromUserByField($userVal,'email');
	if( empty( $r )) { $r = array(); }
	
	$passDbo = $r['pass'];
	$saltDbo = $r['salt'];
	
	//Codifiquem el password del formulari per transformar-lo en format codificat amb el salt corresponent.
	$passForm = encodePassword( $passVal, $saltDbo );
		
	if(strtoupper($passDbo)==strtoupper($passForm)){
		return true;
	}else{
		return false;
	}
}

//evalua si un nou usuari té camps repetits no permesos a la BBDD se li passarà el $_get del formulari 
//retorna true o false;
//alta_DNI 
function evalInsertNewUserUniques($formGet){
	//els parametres que seran evaluats
	$uniquesValues= ['alta_DNI', 'alta_email'];
	$evaluationResult=true;
	foreach($uniquesValues as $value){
		//crida a la funció selectAllFromUserByField amb els camps de formulari i el nom del camp a la BBDD (explode)
		$r=NULL;
		$r=selectAllFromUserByField($formGet[$value], netejaPrefixVariable($value));
		if (isset($r)){
			$evaluationResult=false;
		}
	}
	return $evaluationResult;
}

?>
