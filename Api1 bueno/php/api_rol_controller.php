<?php
 
//**************************************
//Funcións que permeten accés a les pagines
//*************************************
//Control de sessió
function garantAcces(){
	//Si la cookie de sessió no té el camp ['login'] = true, redireccionem al formulari d'accés.
	if((!isset($_SESSION ['login'])) || (isset($_SESSION ['login']) && $_SESSION ['login'] != true)){
			return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
					$_SERVER['SERVER_ADDR'], getBaseURI() ) );
	}
}

//Funció que retorna true si el rol apareix a la clau del array de configuracio
function isRoleAllowedForAction( $destination, $action ) {
	include_once('api_utils.php');
	$rolesAvailables = getAvailableRoles();
	$rol=$_SESSION[ 'user' ][ 'rol' ];
	return in_array( $action ,$rolesAvailables[$rol][$destination] );
}

//Funció que retorna al loggin si no es té permis
function isRoleAllowedForActionOrRedirect($destination, $action){
	if (isRoleAllowedForAction( $destination, $action )){
		return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
					$_SERVER['SERVER_ADDR'], getBaseURI() ) );
	}
}

//Fució que permet al formulari introduir els rols aqui indicats
function getRols(){
	return array('administrator', 'worker', 'user');
}
//Funció que retorna un array amb els permisos d'accés per l'aplicatiu
function getAvailableRoles() {
	return array(
		'administrator' => 	array (
							//permisos al menu de destination
							'menu' => array ('user','price','product','event' ,'order'),
							//permisos al menu de action
							'user' => array ('read','create','update', 'delete'),
							'price' => array ('read','create','update'),
							'product' => array ('read','create','update'),
							'event' => array ('read','create','update', 'sign', 'delete'),
							'order' => array ('read','create','update','delete')
							),
		'worker'=> 	array (
							//permisos al menu de destination
							'menu' => array ('user','price','product','event' ,'order'),
							//permisos al menu de action							
							'user' => array ('read','create','update', 'delete'),
							'price' => array ('read'),
							'product' => array ('read','create'),
							'event' => array ('read','create','update', 'sign'),
							'order' => array ('read','create','update','delete')
							),
		'user'=> 	array (
							//permisos al menu de destination
							'menu' => array ('event','order'),
							//permisos al menu de action							
							'event' => array ('read', 'sign'),
							'order' => array ('readUser', 'createUser','updateUser','delete')
							)
		);
}
?>
