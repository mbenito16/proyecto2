<?php
	//funció que a partir d'un rol d'usuari mostrarà un menú amb les opcions habilitades
	mostraMenuAction($_GET['destination']);
	
	if (!empty($_GET['action'])){
		switch($_GET['action']){
			case 'create': include_once 'user_template_create.php';	
						 break;
			case 'read': include_once 'user_template_read.php';	
						 break;
			case 'update': include_once 'user_template_update_admin.php';	
						 break;
			case 'updateUser': include_once 'user_template_update.php';	
						 break;
			default: return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
						$_SERVER['SERVER_ADDR'], getBaseURI() ));
						break;
		}
	}else{
		include_once 'user_template_read.php';	
	}
?>
