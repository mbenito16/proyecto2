<?php
	include_once('sql_controller.php');
	include_once ( 'api_utils.php' );
	sessionStart();
	//comprovem si l'usuari té la cookie de sessio i redireccionem a sessió privada.
	/*if ( !empty ( $_SESSION [ 'user' ])){
		return header( sprintf( 'Location: http://%s/%sview_acceso.php?user=%s', $_SERVER['SERVER_ADDR'], getBaseURI(), $_SESSION[ 'user' ][ 'name' ] ) );

	}*/
	//Hem fet summit al formulari d'accés??
	
	$userVal = '';
	$msgUser = '';
	$passVal = '';
	$msgPass = '';

	//Si la sessió età activa permetem accés al usuari.
	if( isset ($_SESSION [ 'login' ]) && $_SESSION [ 'login' ] == true ){
		return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=event', 
		$_SERVER['SERVER_ADDR'], getBaseURI()) );
	}
	if( !empty( $_POST[ 'send' ])){
	//Evaluem els valors validats i recomposem el formulari.
		//existeix camp user? assignem user : retornem missatge 'omple user'
		if ( !empty($_POST[ 'login_emailDNI' ] )){
			$userVal = $_POST[ 'login_emailDNI' ];
		}else {
			$msgUser = 'El camp <strong>usuari</strong> no ha estat enviat!';
		}
		//existeix camp password? assignem password : retornem missatge 'omple password'
		if ( !empty ($_POST[ 'login_pass' ])){
			$passVal = $_POST[ 'login_pass' ];
		}else{
			$msgPass = 'El camp <strong>password</strong> no ha estat enviat!';
		}
		//tots els camps omplerts?
		if ( !empty($userVal) && !empty($passVal) ){
			//són correctes? redireccionem a la seccio privada : retornem missatge d'error
			if(evaluaLogin($userVal,$passVal)){	
				//Guardem els parametres de sessio per a controls posteriors.
				$_SESSION [ 'login' ] = true;
				$r = selectAllFromUserByField($userVal, 'email');				
				$_SESSION[ 'user' ][ 'id'] = encryptString($r['id']);
				$_SESSION[ 'user' ][ 'name' ] = encryptString($r['nom']);
				$_SESSION[ 'user' ][ 'surname' ] = encryptString($r['cognom']);
				$_SESSION[ 'user' ][ 'rol' ] = encryptString($r['rolUsuari']);
				return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=event', 
		$_SERVER['SERVER_ADDR'], getBaseURI()) );
			}else{
				$error = "L\'usuari no sha validat correctament!";
			}
		}
	}
	if ( empty ( $_SESSION [ 'login' ])){
		//sessionDestroy();
	}
?>
<!DOCTYPE html>
<head>
<link href="../css/index.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<div id="titulo">
		<p class="titol">DIVEMANAGER</p>
		<div class="boto">
		</div>
	</div>
	<?php
		if( empty( $error ) ) {
			echo '';
		} else { 
			echo '<span style="color:red;"><strong>'.$error.'</strong></span>' . '<span style="display:block; padding-top:5px;"></span>';
			$userVal = '';
			$passVal = '';
		}
	?>
	<div id="cos">
		<h1 class="registre">Accés usuari</h1><br>
			<form action="loggin_controllerTemplate.php" method="POST">
			<div class="input1">
				<label for ='login_emailDNI'>Email o DNI</label>
				<input id="login_emailDNI" name='login_emailDNI' type="text" value="<?php echo $userVal ?>"/>
				<?php echo $msgUser; ?>
			</div>
			<div class="input2">
				<label for = 'login_pass'>Password</label>
				<input id="login_pass" name='login_pass' type="password" value=""/>
				<?php echo $msgPass; ?>
			</div>
			<div class="input3">
				<input name="send" type="submit" value="ENTRAR">
			</div>
	<a href="<?php echo sprintf( 'http://%s/%sregistre_controllerTemplate.php?destination=user&action=create', 
		$_SERVER['SERVER_ADDR'], getBaseURI()); ?>">nou usuari</a>
	</div>
	<!--<div id="footer">
		<p class="peu">Copiryght</p>
	</div>-->
</body>
</html>
