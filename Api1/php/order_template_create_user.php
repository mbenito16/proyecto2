<?php
	$error="";
	//si hem fet submit al formulari
	if( !empty( $_POST[ '_send' ])){
		include_once( 'sql_repository.php' );
		//sanejem entrada
		foreach ($_POST as $k => $v){
			$formValues[$k]= cleanInput($v);			
		}
			InsertNewOrder($formValues);
			return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=order&action=createUser', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));			
	}else{
		
		//generem el contingut buit pels camps value.
		$formValues = Array ( 
			"alta_client_order" => "",
			"alta_materialRetornat_order" => "",
			"alta_pagat_order" => "",
			"alta_esdeveniment_order" => "",
			"alta_preuTotal_order" => ""
		);
		
	}
?>
<form action=<?php echo sprintf("home_controller.php?destination=order&action=createUser");?> method="POST">
	<p><?php echo $error ?></p>
		<div class="input">
					<label for = 'alta_client_order'>Nom Client</label>
					<select id= 'alta_client_order' name='alta_client_order'>		
						<option value="<?php echo ($_SESSION['user']['id']); ?>" selected="selected" > <?php echo ($_SESSION['user']['name'] . " " . $_SESSION['user']['surname'] ); ?></option>
					</select>
			</div>
			<div class="input">
				<input id="alta_materialRetornat_order" name='alta_materialRetornat_order' readonly="readonly" type="hidden" maxlength="25" value="no"/>
			</div>
			<div class="input">
				<input id="alta_pagat_order" name='alta_pagat_order' readonly="readonly" type="hidden" maxlength="25" value="no"/>
			</div>
			<div class="input">
				<label for = 'alta_esdeveniment_order'>Esdeveniments</label>
				<select id= 'alta_esdeveniment_order' name='alta_esdeveniment_order' >
					<p>puta</p>		
					<?php getEventsOrSelected() ?>
				</select>
			</div>
			<br><br>
			<h3 class='subtitle'>Productes</h3>
			<div class="productes">
				<?php getInputsProductes(); ?>
			</div>	
		<br>
	<input name="_send" type="submit" value="RESERVAR">
</form>
