<?php
 include_once( 'api_utils.php' );
 include_once( 'sql_repository2.php' );
 
//*************************************
//********  SELECTS  ******************
//*************************************

 
 //SELECT * FROM users;
function selectAllUsers(){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `usuaris';
	$statement1 =  $dbh->prepare( $query );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return  $r ;
}

//SELECT * FROM preus
function selectAllPreus(){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM preus';
	$statement1 =  $dbh->prepare( $query );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return  $r ;
}

//SELECT * FROM comandes
function selectAllOrder(){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT c.id id, u.nom u_nom, u.cognom u_cognom, e.nom e_nom, c.materialRetornat, c.comandaPagat, e.dataInici from comandes c join usuaris u on c.id_client=u.id join esdeveniments e on c.id_esdeveniment=e.id';
	$statement1 =  $dbh->prepare( $query );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return  $r ;
}

//***************************************************************************************************************************************
 
 // SELECT * FROM users WHERE $camp = $campVal
function selectAllFromUserByField($campVal, $camp){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `usuaris` WHERE `' . $camp . '` = :campVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'campVal', $campVal, \PDO::PARAM_STR  );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);	
	return array_shift( $r );
}


 // SELECT * FROM preus WHERE $camp = $campVal
function selectAllFromPriceByField($campVal, $camp){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `preus` WHERE `' . $camp . '` = :campVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'campVal', $campVal, \PDO::PARAM_STR  );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return array_shift( $r );	
}


 // SELECT * FROM comandes WHERE $camp = $campVal
function selectAllFromOrderByField($campVal, $camp){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `comandes` WHERE `' . $camp . '` = :campVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'campVal', $campVal, \PDO::PARAM_STR  );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return array_shift( $r );	
}

function selectFromProductesReservatsByField($campVal, $camp){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `productesReservats` WHERE `' . $camp . '` = :campVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'campVal', $campVal, \PDO::PARAM_STR  );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	//return array_shift( $r );
	return $r;
}

//***************************************************************************************************************************************


// SELECT * FROM users BY id
function selectAllFromUser($idVal){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `usuaris` WHERE `id` = :idVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'idVal', $idVal, \PDO::PARAM_STR  );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return array_shift( $r );	
}

function selectAllFromPrice($idVal){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT * FROM `preus` WHERE `id` = :idVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'idVal', $idVal, \PDO::PARAM_STR  );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return array_shift( $r );	
}

// SELECT formValues FROM comandes BY id_Client
function selectAllFromOrder($idVal, $z = false){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'SELECT c.id id, u.nom u_nom, u.cognom u_cognom, e.nom e_nom, c.materialRetornat, c.comandaPagat, e.dataInici from comandes c join usuaris u on c.id_client=u.id join esdeveniments e on c.id_esdeveniment=e.id WHERE `id_client` = :idVal;';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'idVal', $idVal, \PDO::PARAM_STR  );
	$statement1->execute();
	if( $z == true ) { return $statement1 ;}
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	unset($mysql);
	return array_shift( $r );	
}

//*************************************
//********  INSERTS  ******************
//*************************************

// Insertar un usuari de tipus client.			
function InsertNewUser($r){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$r['salt'] = base_convert( sha1( uniqid( mt_rand(), true) ), 16, 36 );
	$r['alta_pass'] = encodePassword( $r['alta_pass'], $r['salt'] );
	
	$query = 'INSERT INTO usuaris (nom, cognom, DNI, email, pass, salt, titulacio, 
								nivellTitulacio, numeroTitulacio, adreca, telefon, 
								asseguranca, polissa, caducitat, tarifaClient, 
								punts, rolUsuari, dataModificacio, usuari_modificacio)
					 VALUES 	(:nom, :cognom, :DNI, :email, :pass, :salt, :titulacio, 
								:nivellTitulacio, :numeroTitulacio, :adreca, :telefon, 
								:asseguranca, :polissa, :caducitat, :tarifaClient, 
								:punts, :rolUsuari, :dataModificacio, :usuari_modificacio)';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['alta_nom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'cognom', $r['alta_cognom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'DNI', $r['alta_DNI'], \PDO::PARAM_STR );
	$statement1->bindValue( 'email', $r['alta_email'], \PDO::PARAM_STR );
	$statement1->bindValue( 'pass', $r['alta_pass'], \PDO::PARAM_STR );
	$statement1->bindValue( 'salt', $r['salt'], \PDO::PARAM_STR );
	$statement1->bindValue( 'titulacio', $r['alta_titulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'nivellTitulacio', $r['alta_nivellTitulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'numeroTitulacio', $r['alta_numeroTitulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'adreca', $r['alta_adreca'], \PDO::PARAM_STR );
	$statement1->bindValue( 'telefon', $r['alta_telefon'], \PDO::PARAM_STR );
	$statement1->bindValue( 'asseguranca', $r['alta_asseguranca'], \PDO::PARAM_STR );
	$statement1->bindValue( 'polissa', $r['alta_polissa'], \PDO::PARAM_STR );
	$statement1->bindValue( 'caducitat', $r['alta_caducitat'], \PDO::PARAM_STR );
	$statement1->bindValue( 'tarifaClient', "a", \PDO::PARAM_STR );
	$statement1->bindValue( 'punts', 1, \PDO::PARAM_INT );
	$statement1->bindValue( 'rolUsuari', 'user', \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$var = $statement1->execute();
}



function InsertNewPrice($r){
						//Array del formulari.
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = 'INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
					 VALUES 	(:nom, :info, :preu_a, :preu_b, :preu_c, :preu_d, :dataModificacio, :usuari_modificacio)';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['alta_nom_preu'], \PDO::PARAM_STR );
	$statement1->bindValue( 'info', $r['alta_info_preu'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_a', $r['alta_a_preu'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_b', $r['alta_b_preu'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_c', $r['alta_c_preu'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_d', $r['alta_d_preu'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$var = $statement1->execute();
}

function InsertNewOrder($r){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = 'INSERT INTO comandes (id_client, id_esdeveniment, materialRetornat, comandaPagat, dataModificacio, usuari_modificacio)
					 VALUES 	(:id_client, :id_esdeveniment, :materialRetornat, :comandaPagat, :dataModificacio, :usuari_modificacio)';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'id_client', $r['alta_client_order'], \PDO::PARAM_STR );
	$statement1->bindValue( 'id_esdeveniment', $r['alta_esdeveniment_order'], \PDO::PARAM_STR );
	$statement1->bindValue( 'materialRetornat', $r['alta_materialRetornat_order'], \PDO::PARAM_STR );
	$statement1->bindValue( 'comandaPagat', $r['alta_pagat_order'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$var = $statement1->execute();
	return $dbh->lastInsertId();
}


function InsertOrderProducts($r,$lastInsertId){
	foreach($r as $k => $v){
		if(netejaPrefixVariable($k)== 'order'){
			$mysql = getConnection();
			$dbh = $mysql->getDBH();
			$query = 'INSERT INTO productesReservats (id_comanda, id_preuProducte, numero_productes)
							 VALUES 	(:id_comanda, :id_preuProducte, :numero_productes)';
			$statement1 = $dbh->prepare( $query );
			$statement1->bindValue( 'id_comanda', $lastInsertId, \PDO::PARAM_INT );
			$statement1->bindValue( 'id_preuProducte', netejaSufixVariable($k), \PDO::PARAM_INT );
			$statement1->bindValue( 'numero_productes', $v, \PDO::PARAM_INT );
			$var = $statement1->execute();
		}		
	}
}
//*************************************
//********  UPDATE  ******************
//*************************************

// Modificar un usuari de tipus client.			
function UpdaterNewUser($r,$idUser){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = '	UPDATE usuaris 
				SET nom = :nom , 
					cognom = :cognom, 
					DNI = :DNI, 
					email = :email, 
					titulacio = :titulacio, 
					nivellTitulacio = :nivellTitulacio, 
					numeroTitulacio = :numeroTitulacio, 
					adreca = :adreca, 
					telefon = :telefon,
					asseguranca = :asseguranca, 
					polissa = :polissa, 
					caducitat = :caducitat, 
					tarifaClient = :tarifaClient,
					dataModificacio = :dataModificacio	,
					usuari_modificacio = :usuari_modificacio	
				WHERE id=:idUser' ;
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['nom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'cognom', $r['cognom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'DNI', $r['DNI'], \PDO::PARAM_STR );
	$statement1->bindValue( 'email', $r['email'], \PDO::PARAM_STR );
	$statement1->bindValue( 'titulacio', $r['titulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'nivellTitulacio', $r['nivellTitulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'numeroTitulacio', $r['numeroTitulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'adreca', $r['adreca'], \PDO::PARAM_STR );
	$statement1->bindValue( 'telefon', $r['telefon'], \PDO::PARAM_STR );
	$statement1->bindValue( 'asseguranca', $r['asseguranca'], \PDO::PARAM_STR );
	$statement1->bindValue( 'polissa', $r['polissa'], \PDO::PARAM_STR );
	$statement1->bindValue( 'caducitat', $r['caducitat'], \PDO::PARAM_STR );
	$statement1->bindValue( 'tarifaClient', $r['tarifaClient'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$statement1->bindValue( 'idUser', $idUser, \PDO::PARAM_INT );
	$var = $statement1->execute();
}

// Modificar un usuari de tipus client per un administrador.			
function UpdaterNewUserRol($r,$idUser){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = '	UPDATE usuaris 
				SET nom = :nom , 
					cognom = :cognom, 
					rolUsuari = :rolUsuari,
					DNI = :DNI, 
					email = :email, 
					titulacio = :titulacio, 
					nivellTitulacio = :nivellTitulacio, 
					numeroTitulacio = :numeroTitulacio, 
					adreca = :adreca, 
					telefon = :telefon,
					asseguranca = :asseguranca, 
					polissa = :polissa, 
					caducitat = :caducitat, 
					tarifaClient = :tarifaClient,
					dataModificacio = :dataModificacio	,
					usuari_modificacio = :usuari_modificacio	
				WHERE id=:idUser' ;
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['nom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'cognom', $r['cognom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'rolUsuari', $r['rol'], \PDO::PARAM_STR );
	$statement1->bindValue( 'DNI', $r['DNI'], \PDO::PARAM_STR );
	$statement1->bindValue( 'email', $r['email'], \PDO::PARAM_STR );
	$statement1->bindValue( 'titulacio', $r['titulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'nivellTitulacio', $r['nivellTitulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'numeroTitulacio', $r['numeroTitulacio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'adreca', $r['adreca'], \PDO::PARAM_STR );
	$statement1->bindValue( 'telefon', $r['telefon'], \PDO::PARAM_STR );
	$statement1->bindValue( 'asseguranca', $r['asseguranca'], \PDO::PARAM_STR );
	$statement1->bindValue( 'polissa', $r['polissa'], \PDO::PARAM_STR );
	$statement1->bindValue( 'caducitat', $r['caducitat'], \PDO::PARAM_STR );
	$statement1->bindValue( 'tarifaClient', $r['tarifaClient'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$statement1->bindValue( 'idUser', $idUser, \PDO::PARAM_INT );
	$var = $statement1->execute();
}

function UpdaterNewPrice($r,$idPrice){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = '	UPDATE preus 
				SET nom = :nom , 
					info = :info, 
					preu_a = :preu_a, 
					preu_b = :preu_b,
					preu_c = :preu_c,
					preu_d = :preu_d,
					dataModificacio = :dataModificacio,
					usuari_modificacio = :usuari_modificacio
					WHERE id=:idPrice';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['nom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'info', $r['info'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_a', $r['a'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_b', $r['b'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_c', $r['c'], \PDO::PARAM_STR );
	$statement1->bindValue( 'preu_d', $r['d'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$statement1->bindValue( 'idPrice', $idPrice, \PDO::PARAM_INT );
	$var = $statement1->execute();
}


function UpdaterNewOrder($r,$idOrder){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = '	UPDATE comandes 
				SET id_client = :id_client , 
					id_esdeveniment = :id_esdeveniment, 
					materialRetornat = :materialRetornat,
					comandaPagat = :comandaPagat,
					dataModificacio = :dataModificacio,
					usuari_modificacio = :usuari_modificacio
					WHERE id=:idOrder';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'id_client', $r['update_client_order'], \PDO::PARAM_INT );
	$statement1->bindValue( 'id_esdeveniment', $r['update_esdeveniment_order'], \PDO::PARAM_INT );
	$statement1->bindValue( 'materialRetornat', $r['update_materialRetornat_order'], \PDO::PARAM_STR );
	$statement1->bindValue( 'comandaPagat', $r['update_pagat_order'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$statement1->bindValue( 'idOrder', $idOrder, \PDO::PARAM_INT );
	$var = $statement1->execute();
}

function UpdateOrderProducts($r,$idOrder){
	//print_r($r);die();
	foreach($r as $k => $v){
		if(netejaPrefixVariable($k)== 'order'){
			$mysql = getConnection();
			$dbh = $mysql->getDBH();
			$query = '	UPDATE productesReservats
						SET id_comanda = :id_comanda,
							id_preuProducte = :id_preuProducte,
							numero_productes = :numero_productes
							WHERE id_comanda = :idOrder 
							AND id_preuProducte = :id_preusProducte' ;
			$statement1 = $dbh->prepare( $query );
			$statement1->bindValue( 'id_comanda', $idOrder, \PDO::PARAM_INT );
			$statement1->bindValue( 'id_preuProducte', netejaSufixVariable($k), \PDO::PARAM_INT );
			$statement1->bindValue( 'numero_productes', $v, \PDO::PARAM_INT );
			$statement1->bindValue( 'idOrder', $idOrder, \PDO::PARAM_INT );
			$statement1->bindValue( 'id_preusProducte', netejaSufixVariable($k), \PDO::PARAM_INT );
			$var = $statement1->execute();
		}		
	}
}

//*************************************
//********  DELETE   ******************
//*************************************

function deleteEvent($idEvent){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = 'DELETE FROM esdeveniments WHERE id= :idEvent;';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'idEvent', $idEvent, \PDO::PARAM_INT );
	$var = $statement1->execute();	
}

function deleteOrder($idComanda){
	deleteProductesReservats($idComanda);
	
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'DELETE FROM comandes WHERE id= :idComanda;';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'idComanda', $idComanda, \PDO::PARAM_INT );
	$var = $statement1->execute();	
}

function deleteProductesReservats($idComanda){
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$query = 'DELETE FROM productesReservats WHERE id_comanda= :idComanda;';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'idComanda', $idComanda, \PDO::PARAM_INT );
	$var = $statement1->execute();	
}
?>

