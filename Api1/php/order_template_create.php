<?php
	$error="";
	//si hem fet submit al formulari
	
	if( !empty( $_POST[ '_send' ])){
		include_once( 'sql_repository.php' );
		//sanejem entrada
		foreach ($_POST as $k => $v){
			$formValues[$k]= cleanInput($v);			
		}
			$lastInsertId = InsertNewOrder($formValues);
			InsertOrderProducts($formValues, $lastInsertId);
			return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=order&action=create', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));			
	}else{
		
		//generem el contingut buit pels camps value.
		$formValues = Array ( 
			"alta_client_order" => "",
			"alta_materialRetornat_order" => "",
			"alta_pagat_order" => "",
			"alta_esdeveniment_order" => "",
			"alta_preuTotal_order" => ""
		);
		
	}
?>

<form action=<?php echo sprintf("home_controller.php?destination=order&action=create");?> method="POST">
<p><?php echo $error ?></p>
	<div class="input">
		<label for = 'alta_client_order'>Nom Client</label>
		<select id= 'alta_client_order' name='alta_client_order'>		
			<?php getUsers(); ?>
		</select>
		</div>
	<div class="input">
		<label for = 'alta_materialRetornat_order'>Material</label>
		<select id= 'alta_materialRetornat_order' name='alta_materialRetornat_order' >	
				<option value="no" > Llogat </option>	
				<option value="si" > Tornat </option>
		</select>
	</div>
	<div class="input">
		<label for = 'alta_pagat_order'>Pagat</label>
		<select id= 'alta_pagat_order' name='alta_pagat_order' >
			<option value="no" > No pagat </option>		
			<option value="si" > Pagat </option>			
		</select>
	</div>
	<div class="input">
		<label for = 'alta_esdeveniment_order'>Esdeveniments</label>
		<select id= 'alta_esdeveniment_order' name='alta_esdeveniment_order'>		
			<?php getEvents(); ?>
		</select>
	</div>	
	<br><br>
	<h3 class='subtitle'>Productes</h3>
	<div class="productes">
		<?php getInputsProductes(); ?>
	</div>	
	<input name="_send" type="submit" value="RESERVAR">
</form>
