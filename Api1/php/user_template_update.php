<?php
	include_once( 'sql_repository.php' );
	include_once( 'sql_controller.php' );
	
	$valid = true;
	//Hem fet submit
	if( !empty( $_POST[ '_send' ])){
	//sanejem entrada
		foreach ($_POST as $k => $v){
			$k = netejaPrefixVariable($k);
			$formValues[$k]= cleanInput($v);			
		}
	//Busquem els usuaris que tenen el DNI com el modificat
		$rCheck = selectAllFromUserByField($formValues['DNI'], 'DNI');
		//Si el DNI modificat esta a la base de dades amb un altre ID no es podrà utilitzar
		if ( isset($rCheck) && ($rCheck['id'] != $_GET['id'])) {
			$valid = false;
			$errors['DNI'] = 'Ja existeix un usuari amb aquest DNI';
		}
		
	//Busquem els usuaris que tenen el DNI com el modificat
		$rCheck = selectAllFromUserByField($formValues['email'],'email');
		//Si el EMAIL modificat esta a la base de dades amb un altre ID no es podrà utilitzar
		if ( isset($rCheck) && ( $rCheck['id'] != $_GET['id'] )) {//
			$valid = false;
			$errors['email'] = 'Ja existeix un usuari amb aquest email';
		}
		//Si ha passat els controls modifiquem el usuari
		if($valid == true){
			UpdaterNewUser($formValues,$_GET['id']);
			return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=user&action=read', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));
		}
	}else if(empty( $_POST[ 'send' ])){
		if( empty( $_POST[ 'send' ])){
			$errors="";
			$r = selectAllFromUserByField($_GET['id'],"id");
			$formValues = $r;
		}
	}
?>
	<form action=<?php echo sprintf("home_controller.php?destination=user&action=update&id=%d",$_GET['id']);?> method="POST">
		<p><?php print_r(@$errors['DNI']); ?></p>
		<p><?php print_r(@$errors['email']); ?></p>
		<div class="input">
			<label for = 'update_nom'>nom</label>
			<input id="update_nom" name='update_nom' required type="text" maxlength="25" value="<?php echo $formValues['nom'];?>"/>
		</div>
		<div class="input">
			<label for ='alta_cognom'>cognom</label>
			<input id="update_cognom" name='update_cognom' required type="text" maxlength="100" value="<?php echo $formValues['cognom'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_DNI'>DNI</label>
			<input id="update_DNI" name='update_DNI' required type="dni" maxlength="12" value="<?php echo $formValues['DNI'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_email'>email</label>
			<input id="update_email" name='update_email' required type="email" maxlength="100"  value="<?php echo $formValues['email'];?>"/>
		</div>	
		<div class="input">
			<label for = 'update_titulacio'>titulacio</label>
			<input id="update_titulacio" name='update_titulacio' type="text" maxlength="100" value="<?php echo $formValues['titulacio'];?>"/>
		</div>	
		<div class="input">
			<label for = 'update_nivellTitulacio'>nivell de titulacio</label>
			<input id="update_nivellTitulacio" name='update_nivellTitulacio' type="text" maxlength="100" value="<?php echo $formValues['nivellTitulacio'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_numeroTitulacio'>numero de titulacio</label>
			<input id="update_numeroTitulacio" name='update_numeroTitulacio' type="text" maxlength="100" value="<?php echo $formValues['numeroTitulacio'];?>"/>
		</div>	
		<div class="input">
			<label for = 'update_adreca'>adreca</label>
			<input id="update_adreca" name='update_adreca' type="text" maxlength="100" value="<?php echo $formValues['adreca'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_telefon'>telefon</label>
			<input id="update_telefon" name='update_telefon' type="tel" maxlength="15" value="<?php echo $formValues['telefon'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_asseguranca'>asseguranca</label>
			<input id="update_asseguranca" name='update_asseguranca' type="text" maxlength="100" value="<?php echo $formValues['asseguranca'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_polissa'>polissa</label>
			<input id="update_polissa" name='update_polissa' type="text" maxlength="50" value="<?php echo $formValues['polissa'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_caducitat'>caducitat</label>
			<input id="update_caducitat" name='update_caducitat' type="date" value="<?php echo $formValues['caducitat'];?>"/>
		</div>
		<div class="input">
			<label for = 'update_tarifaClient'>tarifa client</label>
			<input id="update_tarifaClient" name='update_tarifaClient' type="text" value="<?php echo $formValues['tarifaClient'];?>"/>
		</div>
		<input name="_send" type="submit" value="Modificar"/>
	</form>
