<?php
	include_once( 'sql_repository2.php' );
	include_once( 'sql_controller.php' );
	if(!empty($_POST['_send'])){
		//foreach per a sanejar inputs formulari
		foreach($_POST as $k=>$v){
			$k = netejaPrefixVariable($k);
			$formValues[$k] = cleanInput($v);
		}
		UpdaterNewEvents($formValues,$_GET['id']);
		return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=event&action=read', 
						$_SERVER['SERVER_ADDR'], getBaseURI() ));
	}	else if(empty( $_POST[ 'send' ])){
		if( empty( $_POST[ 'send' ])){
			$errors="";
			$r = selectAllFromEventsByField($_GET['id'],"id");
			$formValues = $r;
		}
	}

?>
<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
$(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'
	});
  });
  </script>
</head>
		<form action=<?php echo sprintf("home_controller.php?destination=event&action=update&id=%d",$_GET['id']);?> method="POST">
			<div class="input">
				<label for = 'update_nom_event'>Nom</label>
				<input id="update_nom_event" name='update_nom_event' required type="text" maxlength="25" value="<?php echo $formValues['nom'];?>"/>
			</div>
			<div class="input">
				<label for = 'update_sortida_event'>Lloc sortida</label>
				<input id="update_sortida_event" name='update_sortida_event' required type="text" maxlength="25" value="<?php echo $formValues['lloc_sortida'];?>"/>
			</div>	
			<div class="input">
				<label for = 'update_inici_event'>Data inici</label>
				<input id="datepicker" name='update_inici_event' type="date" value="<?php echo $formValues['dataInici'];?>"/>
			</div>	
		<br>
		<input name="_send" type="submit" value="Modificar">
</form>
</html>
