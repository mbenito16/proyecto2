<?php
include_once ( 'bdd_connectObject.php' );
include_once ( 'api_rol_controller.php' );
include_once ( 'api_templates.php' );

function print_rForm($valor){
	echo sprintf('<p>%s</p>',$valor);
}
//*************************************
//Sessions i coneccions
//*************************************
//Iniciar sessio.
function sessionStart(){
	session_start();
	//Al fer sessionStart() els valors de les cookies de sessio no estan al servidor per a incloure'ls farem la assignació següent.
	if( empty( $_COOKIE ) )  {
		$_COOKIE[ session_name() ] = session_id();
	}
}

//Tanca sessio
function sessionDestroy(){
	//Esborrem els valors de la cookie de sessio del servidor.
	$_SESSION = array();
	if( isset( $_COOKIE[ session_name() ] ) ) {
		unset( $_COOKIE[ 'PHPSESSID' ] );
		setcookie( session_name(), '', time() - 42000, '/');
	}
	$_COOKIE = array();
	session_destroy();
	
	//Les variables globals $_SESSION i $_COOKIE estan intactes
	
}
//Finalitzar sessió.
function logout() {
	sessionDestroy();      
	return header( sprintf('Location: http://%s/%sloggin_controllerTemplate.php', $_SERVER['SERVER_ADDR'], getBaseURI() ) );  
}

//Funcio que genera una conecció a BDD amb PDO.
function getConnection(){
	$mysql = new \MySQL_Func();
	//recullim EN UNA PROPIETAT de $mysql un objecte de tipus PDO que ens permetrà fer procediments de les conxions
	return $mysql->connect();
} 

//*******FUNCIONS AUXILIARS*************
//Funcio que codifica un password en clar i el transforma codificat
function encodePassword( $passEnClaro, $salt ){
	//Definim l'algoritme a utilitzar
	$algorithm = 'sha1';

	//Definim el nombre d'iteracions per encriptar el password
	$iterations = 1;

	//No volem codificar a base64
	$encodeHashAsBase64 = false;

	//Podem comprobar si el password és massa llarg

	$salted = mergePasswordAndSalt( $passEnClaro, $salt );
	$digest = hash( $algorithm, $salted, true ); //------------------------------------??¿??

	// "stretch" hash
	for( $i = 1; $i < $iterations; ++$i ) {
		$digest = hash( $algorithm, $digest.$salted, true );
	}

	return $encodeHashAsBase64 ? base64_encode( $digest ) : bin2hex( $digest );
}

//Funcio que evalua un string corresponent a un password i 
function mergePasswordAndSalt( $password, $salt )
    {
        if ( empty( $salt ) ) {
           return $password;//-----------------------------?¿?¿?
        }

        if( false !== strrpos( $salt, '{' ) || false !== strrpos( $salt, '}' ) ) {
            throw new \InvalidArgumentException( 'Cannot use { or } in salt.' );
        }

        return $password.'{'.$salt.'}';
    }
    
//Funció utilitzada per a redireccionar, ens retorna el directori pare del fitxer al servidor.
function getBaseURI() {
            $s = strrpos( $_SERVER[ 'SCRIPT_NAME' ], '/' );
            return substr( $_SERVER[ 'SCRIPT_NAME' ], 0, $s ) . DIRECTORY_SEPARATOR;
}

//Funció que retorna el fitxer .php que estem executant
function getPage() {
	$length = strpos( $_SERVER[ 'SCRIPT_FILENAME' ], '.php');
	$str    = substr( $_SERVER[ 'SCRIPT_FILENAME' ], 0, $length );
	$start  = strrpos( $str, '/');
	return substr( $_SERVER[ 'SCRIPT_FILENAME' ], $start+1, $length );
}

//Funcio que saneja formularis
function cleanInput( $input ){
	return $input;
}

//Funció que elimina el prefix en un nom de variable tipus prefix_nom
function netejaPrefixVariable( $variableAmbPrefix ){
		$variableAmbPrefix = explode("_", $variableAmbPrefix)[1];
		return $variableAmbPrefix;
}

//Funció que elimina el sufix en un nom de variable tipus prefix_nom
function netejaSufixVariable( $variableAmbPrefix ){
		$variableAmbPrefix = explode("_", $variableAmbPrefix)[2];
		return $variableAmbPrefix;
}

//Funcio que encripta un string
function encryptString( $string ){
	return $string;
}
//Funció que desencripta un string
function decryptString( $string ){
	return $string;
}
?>


