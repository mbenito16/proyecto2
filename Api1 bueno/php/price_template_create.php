<?php
	$error="";
	//si hem fet submit al formulari
	if( !empty( $_POST[ '_send' ])){
		include_once( 'sql_repository.php' );
		include_once( 'sql_controller.php' );
		//sanejem entrada
		foreach ($_POST as $k => $v){
			$formValues[$k]= cleanInput($v);			
		}
			InsertNewPrice($formValues);

			return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=price&action=read', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));			
	}else{
		
		//generem el contingut buit pels camps value.
		$formValues = Array ( 
			"alta_nom_preu" => "",
			"alta_info_preu" => "",
			"alta_a_preu" => "",
			"alta_b_preu" => "",
			"alta_c_preu" => "",
			"alta_d_preu" => ""
		);
		
	}
?>


<form action=<?php echo sprintf("home_controller.php?destination=price&action=create",$_SESSION[ 'user' ][ 'name' ]);?> method="POST">
<p><?php echo $error ?></p>
<div class="input">
	<label for = 'alta_nom_preu'>Nom generic de producte</label>
	<input id="alta_nom_preu" name='alta_nom_preu' required type="text" maxlength="99" placeholder="Introdueix aqui el nom" value="<?php echo $formValues['alta_nom_preu'];?>"/>
</div>
<div class="input">
	<label for = 'alta_info_preu'>Informacio</label>
	<input id="alta_info_preu" name='alta_info_preu' required type="text" maxlength="99" placeholder="Introdueix aqui talla" value="<?php echo $formValues['alta_info_preu'];?>"/>
</div>
<div class="input">
	<label for = 'alta_a_preu'>Preu A</label>
	<input id="alta_a_preu" name='alta_a_preu' required type="text" maxlength="5" value="<?php echo $formValues['alta_a_preu'];?>"/>
</div>	
<div class="input">
	<label for = 'alta_b_preu'>Preu B</label>
	<input id="alta_b_preu" name='alta_b_preu' required type="text" maxlength="5" value="<?php echo $formValues['alta_b_preu'];?>"/>
</div>
<div class="input">
	<label for = 'alta_c_preu'>Preu C</label>
	<input id="alta_c_preu" name='alta_c_preu' required type="text" maxlength="5" value="<?php echo $formValues['alta_c_preu'];?>"/>
</div>		
<div class="input">
	<label for = 'alta_d_preu'>Preu D</label>
	<input id="alta_d_preu" name='alta_d_preu' type="text"maxlength="1" value="<?php echo $formValues['alta_d_preu'];?>"/>
</div>	
<input name="_send" type="submit" value="REGISTRAR">

