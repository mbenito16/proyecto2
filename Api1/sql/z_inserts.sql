INSERT INTO usuaris (nom, cognom, DNI, email, pass, salt, titulacio, nivellTitulacio, numeroTitulacio, adreca, telefon, asseguranca, polissa, caducitat, tarifaClient, punts, rolUsuari, dataModificacio, usuari_modificacio)
VALUES 	('Pere', 'lagranje', '12345678a', 'pere@pere.pere', '8f4261d2ed9e368046a936325616da0cf31345e7', 'q4zs74v20wg84cooswgg8gcwsg0c0o4', 'PADI', '5*', '123456', 'Carrer gran nº4, Blanes', '606123456', 'axa', '123456', '15/12/2016', 'a', 1, 'administrator', '2016-06-09 02:02:48', ' ');
INSERT INTO usuaris (nom, cognom, DNI, email, pass, salt, titulacio, nivellTitulacio, numeroTitulacio, adreca, telefon, asseguranca, polissa, caducitat, tarifaClient, punts, rolUsuari, dataModificacio, usuari_modificacio)
VALUES 	('Hilai', 'lagranje', '12345678b', 'hilai@hilai.hilai', '8f4261d2ed9e368046a936325616da0cf31345e7', 'q4zs74v20wg84cooswgg8gcwsg0c0o4', 'PADI', '5*', '123456', 'Carrer gran nº4, Blanes', '606123456', 'axa', '123456', '15/12/2016', 'a', 1, 'worker', '2016-06-09 02:02:48', ' ');
INSERT INTO usuaris (nom, cognom, DNI, email, pass, salt, titulacio, nivellTitulacio, numeroTitulacio, adreca, telefon, asseguranca, polissa, caducitat, tarifaClient, punts, rolUsuari, dataModificacio, usuari_modificacio)
VALUES 	('Carla', 'Romani', '12345678c', 'carla@carla.carla', '8f4261d2ed9e368046a936325616da0cf31345e7', 'q4zs74v20wg84cooswgg8gcwsg0c0o4', 'PADI', '5*', '123456', 'Carrer gran nº4, Blanes', '606123456', 'axa', '123456', '15/12/2016', 'a', 1, 'user', '2016-06-09 02:02:48', ' ');
			
INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
VALUES 	('ampolla -15', ' ', '10', '11', '13', '0', '2016-06-09 02:06:20', 'Pere lagranje');
INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
VALUES 	('ampolla -12', ' ', '9', '10', '12', '0', '2016-06-09 02:06:20', 'Pere lagranje');
INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
VALUES 	('traje', ' ', '7', '9', '10', '0', '2016-06-09 02:06:20', 'Pere lagranje');
INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
VALUES 	('sortida barco', 'standard', '18', '20', '22', '0', '2016-06-09 02:06:20', 'Pere lagranje');
INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
VALUES 	('curs classe practica', 'una immersió', '30', '35', '40', '0', '2016-06-09 02:06:20', 'Pere lagranje');
INSERT INTO preus (nom, info, preu_a, preu_b, preu_c, preu_d, dataModificacio, usuari_modificacio)
VALUES 	('bateig', 'platja', '50', '55', '60', '0', '2016-06-09 02:06:20', 'Pere lagranje');

INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('Ampolla - 15', '2016/10/25', 'disponible', 'numero-1', '1', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('Ampolla - 15', '2016/10/25', 'no disponible', 'numero2', '1', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('Ampolla - 12', '2016/10/25', 'disponible', 'numero-1', '2', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('Ampolla - 12', '2016/10/25', 'no disponible', 'numero2', '2', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('traje', '', 'disponible', 'numero-1', '3', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('traje', '', 'disponible', 'numero-2', '3', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('sortida barco', '', 'disponible', ' ', '4', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('sortida zodiac', '', 'disponible', ' ', '4', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('classe 1h', '', 'disponible', 'PADI 1*', '5', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('classe 1h', '', 'disponible', 'PADI 2*', '5', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('classe 1h', '', 'disponible', 'PADI 3*', '5', '2016-06-09 02:15:53', 'Pere lagranje');
INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
VALUES 	('bateig', '', 'disponible', ' ', '6', '2016-06-09 02:15:53', 'Pere lagranje');


INSERT INTO esdeveniments (nom, lloc_sortida, dataInici, dataModificacio, usuari_modificacio)
VALUES 	('Particular', ' ', ' ', '2016-06-09 02:18:09', 'Pere lagranje');
INSERT INTO esdeveniments (nom, lloc_sortida, dataInici, dataModificacio, usuari_modificacio)
VALUES 	('Sortida barca', 'Maria', '2016/12/12', '2016-06-09 02:18:09', 'Pere lagranje');
INSERT INTO esdeveniments (nom, lloc_sortida, dataInici, dataModificacio, usuari_modificacio)
VALUES 	('Bateig', 'Santa Anna', '2016/12/12', '2016-06-09 02:18:09', 'Pere lagranje');
INSERT INTO esdeveniments (nom, lloc_sortida, dataInici, dataModificacio, usuari_modificacio)
VALUES 	('Sortida barca', 'Maria', '2016/12/12', '2016-06-09 02:18:09', 'Pere lagranje');
INSERT INTO esdeveniments (nom, lloc_sortida, dataInici, dataModificacio, usuari_modificacio)
VALUES 	('Sortida barca', 'Maria', '2016/12/12', '2016-06-09 02:18:09', 'Pere lagranje');
