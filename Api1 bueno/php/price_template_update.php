<?php
	include_once( 'sql_repository.php' );
	include_once( 'sql_controller.php' );
	if(!empty($_POST['_send'])){
		//foreach per a sanejar inputs formulari
		
		foreach($_POST as $k=>$v){
			$k = netejaPrefixVariable($k);
			$formValues[$k] = cleanInput($v);
		}
		print_r($formValues);
		UpdaterNewPrice($formValues,$_GET['id']);
		return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=price&action=read', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));
	}	else if(empty( $_POST[ 'send' ])){
		if( empty( $_POST[ '_send' ])){
			$errors="";
			$r = selectAllFromPriceByField($_GET['id'],"id");
			$formValues = $r;	
			//print_r($formValues);		
		}
	}

?>
<form action=<?php echo sprintf("home_controller.php?destination=price&action=update&id=%s", $_GET[ 'id' ]);?> method="POST">
	<div class="input">
		<label for = 'alta_nom_preu'>Nom generic de producte</label>
		<input id="alta_nom_preu" name='alta_nom_preu' required type="text" maxlength="99" placeholder="Introdueix aqui el nom" value="<?php echo $formValues['nom'];?>"/>
	</div>
	<div class="input">
		<label for = 'alta_info_preu'>Informacio</label>
		<input id="alta_info_preu" name='alta_info_preu' required type="text" maxlength="99" placeholder="Introdueix aqui talla" value="<?php echo $formValues['info'];?>"/>
	</div>
	<div class="input">
		<label for = 'alta_a_preu'>Preu A</label>
		<input id="alta_a_preu" name='alta_a_preu' required type="text" maxlength="5" value="<?php echo $formValues['preu_a'];?>"/>
	</div>	
	<div class="input">
		<label for = 'alta_b_preu'>Preu B</label>
		<input id="alta_b_preu" name='alta_b_preu' required type="text" maxlength="5" value="<?php echo $formValues['preu_b'];?>"/>
	</div>
	<div class="input">
		<label for = 'alta_c_preu'>Preu C</label>
		<input id="alta_c_preu" name='alta_c_preu' required type="text" maxlength="5" value="<?php echo $formValues['preu_c'];?>"/>
	</div>		
	<div class="input">
		<label for = 'alta_d_preu'>Preu D</label>
		<input id="alta_d_preu" name='alta_d_preu' type="text"maxlength="1" value="<?php echo $formValues['preu_d'];?>"/>
	</div>	
	<input name="_send" type="submit" value="REGISTRAR">
</form>

