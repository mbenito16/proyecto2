<?php
//OBJECTE CONECCIÓ A LA BASE DE DADES:
Class MySQL_Func
		//Metodes per accedir a base de dades.
{

//PROPIETATS-___________________________________________________________

	//protected per a que no sigui accessible desde fora de la classe.
	protected $driver;
	protected $host;
	protected $port;
	protected $dbName;
	protected $user;
	protected $password;
	protected $dsn;
	protected $dbh;
	protected $options = array();
	
//CONSTRUCTOR___________________________________________________________

	public function __construct( $utf8 = true ) {
						//Constructor
									//es requereix un parametre que si no es passa per defecte està definit com a true.
		include( 'api_config.php' );

		$this->driver   = $config[ 'mysql' ][ 'database_driver' ];
		$this->host     = $config[ 'mysql' ][ 'database_host' ];
		$this->port     = $config[ 'mysql' ][ 'database_port' ];
		$this->dbName   = $config[ 'mysql' ][ 'database_name' ];
		$this->user     = $config[ 'mysql' ][ 'database_user' ];
		$this->password = $config[ 'mysql' ][ 'database_password' ];

		$this->dsn      = sprintf( 'mysql:host=%s;dbname=%s', $this->host, $this->dbName );
										//Aquest string es nescessari per conectarse al MySql que substitueix el mysql_connect per un metode que genera un objete i aporta funcionalitats molt interessants.
		if( $utf8 ) {
			//Setting the connection character set to UTF-8
			$this->options = array(
				\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
						//Constante de la classe PDO que php porta per defecte.
			); 
		}
	}
	
//GETTERS & SETTERS_____________________________________________________

	public function getDriver() {
		return $this->driver;
	}

	public function getHost() {
		return $this->host;
	}

	public function getPort() {
		return $this->port;
	}

	public function getDBName() {
		return $this->dbName;
	}

	public function getUser() {
		return $this->user;
	}

	public function getPassword() {
		return $this->password;
	}

	public function getDBH() {
		return $this->dbh;
	}

	public function setDSN( $dsn ) {
		$this->dsn = $dsn;
		return $this;
	}

//METODES_______________________________________________________________
	
	//Conecta a base de dades i retorna un PDO
	public function connect() {
				try {
					$this->dbh = new \PDO( $this->dsn, $this->user, $this->password, $this->options );
							//Un cop hem obert la conecció tenim informació aqui dins (podriem dir que es la coneccio)
										//Objecte que ja té php per defecte
				} catch ( \PDOException $e ) {
					throw new \Exception( sprintf( 'Connection failed: %s', $e->getMessage() ) );
				}

				return $this;
			}
	
	
	
}
?>


