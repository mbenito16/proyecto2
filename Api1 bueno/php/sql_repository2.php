<?php
 include_once( 'api_utils.php' );
 
 function getArrayForm($querySelect, $camps){
							//$querySelect = 'DESC users;';
										//$camps = array ( 'username', 'password', 'nif', 'name', 'surname1','surname2', 'phone', 'enabled' );
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$statement1 = $dbh->prepare( $querySelect );
	$statement1->execute();
	$errors = Array ();
	//recullim en un array els parametres de la taula i els tipus de dades de mysql que contenen
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );	
		$tmp = $r;
		$r = Array ();

		// Omplim l'array amb els camps del 'DESC users' que tenim a $camps
		foreach ( $tmp as $t ) {
			if ( in_array ( $t['Field'] , $camps ) ) {
				$r[] = $t;
			}
		}
	return $r ;
}
 //retorna tots el camps dels events desde la data actual.
 function selectAllEvents(){
	 $mysql = getConnection();
	 $now = new \Datetime( 'now' );
	//Com que les prpoietats de $mysql són privades fem servir el getDBH per accedir a la propietat $dbh (tipus PDO)
	$dbh = $mysql->getDBH();
	//guardem la query en una variable amb un parametre (:username) que recull una variable. 
	$query = 'SELECT * FROM `esdeveniments` WHERE dataInici >=:dataActual OR dataInici="0000-00-00"';
	$statement1 =  $dbh->prepare( $query );
	$statement1->bindValue( 'dataActual', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	//Eliminem l'objecte de coneccio
	unset($mysql);

	//array_shift per a eliminar la matriu i deixar un array asociatiu normal.
	return  $r ; 
	}
 
 function selectAllFromEventsByField($campVal, $camp){
	
	//Creeem un objecte de coneccions a BBDD
	$mysql = getConnection();
	//Com que les prpoietats de $mysql són privades fem servir el getDBH per accedir a la propietat $dbh (tipus PDO)
	$dbh = $mysql->getDBH();
	//guardem la query en una variable amb un parametre (:username) que recull una variable. 
	$query = 'SELECT * FROM `esdeveniments` WHERE `' . $camp . '` = :campVal;';
															//VARIABLE AMB :
	//per tal de poder recullir una variable dintre de la query anterior cal que cridem a la funció tipus DBO->prepare($query) que retorna un objecte
	//	de tipus statement que té els seus propis metodes per a preparar la query.
	$statement1 =  $dbh->prepare( $query );
	//fent servir mètodes de statement modifiquem la query afegint el valor del parametre que portarà la query.
	$statement1->bindValue( 'campVal', $campVal, \PDO::PARAM_STR  );
	$statement1->execute();
	//guardarem el resultat de la query dins d'una variable que contindrà un array.
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	//Eliminem l'objecte de coneccio
	unset($mysql);
	//array_shift per a eliminar la matriu i deixar un array asociatiu normal.
	return array_shift( $r );	
}

function selectAllFromEvents($idVal){
	
	//Creeem un objecte de coneccions a BBDD
	$mysql = getConnection();
	//Com que les prpoietats de $mysql són privades fem servir el getDBH per accedir a la propietat $dbh (tipus PDO)
	$dbh = $mysql->getDBH();
	//guardem la query en una variable amb un parametre (:username) que recull una variable. 
	$query = 'SELECT * FROM `esdeveniments` WHERE `id` = :idVal;';
															//VARIABLE AMB :
	//per tal de poder recullir una variable dintre de la query anterior cal que cridem a la funció tipus DBO->prepare($query) que retorna un objecte
	//	de tipus statement que té els seus propis metodes per a preparar la query.
	$statement1 =  $dbh->prepare( $query );
	//fent servir mètodes de statement modifiquem la query afegint el valor del parametre que portarà la query.
	$statement1->bindValue( 'idVal', $idVal, \PDO::PARAM_STR  );
	//executa la consulta a una base de dades.
	$statement1->execute();
	//guardarem el resultat de la query dins d'una variable que contindrà un array.
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	//Eliminem l'objecte de coneccio
	unset($mysql);
	//array_shift per a eliminar la matriu i deixar un array asociatiu normal.
	return array_shift( $r );	
}

function InsertNewEvents($r){
	//Array del formulari.
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = 'INSERT INTO esdeveniments (nom, lloc_sortida, dataInici, dataModificacio, usuari_modificacio)
					 VALUES 	(:nom, :lloc_sortida, :dataInici, :dataModificacio, :usuari_modificacio)';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['alta_nom_event'], \PDO::PARAM_STR );
	$statement1->bindValue( 'lloc_sortida', $r['alta_sortida_event'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataInici', $r['alta_inici_event'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$var = $statement1->execute();
}

function UpdaterNewEvents($r,$idEvents){
//Array del formulari.
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = '	UPDATE esdeveniments
				SET nom = :nom , 
					lloc_sortida = :lloc_sortida, 
					dataInici = :dataInici, 
					dataModificacio = :dataModificacio,
					usuari_modificacio = :usuari_modificacio
					WHERE id=:idEvents';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['nom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'lloc_sortida', $r['sortida'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataInici', $r['inici'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$statement1->bindValue( 'idEvents', $idEvents, \PDO::PARAM_INT );
	$var = $statement1->execute();
}



//SELECT * FROM productes
function selectAllProducts(){
	//Creeem un objecte de coneccions a BBDD
	$mysql = getConnection();
	//Com que les prpoietats de $mysql són privades fem servir el getDBH per accedir a la propietat $dbh (tipus PDO)
	$dbh = $mysql->getDBH();
	//guardem la query en una variable amb un parametre (:username) que recull una variable. 
	$query = 'SELECT * FROM productes';
	$statement1 =  $dbh->prepare( $query );
	$statement1->execute();
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	//Eliminem l'objecte de coneccio
	unset($mysql);

	//array_shift per a eliminar la matriu i deixar un array asociatiu normal.
	return  $r ;
}

 // SELECT * FROM product WHERE $camp = $campVal
function selectAllFromProductByField($campVal, $camp){
	
	//Creeem un objecte de coneccions a BBDD
	$mysql = getConnection();
	//Com que les prpoietats de $mysql són privades fem servir el getDBH per accedir a la propietat $dbh (tipus PDO)
	$dbh = $mysql->getDBH();
	//guardem la query en una variable amb un parametre (:username) que recull una variable. 
	$query = 'SELECT * FROM `productes` WHERE `' . $camp . '` = :campVal;';
															//VARIABLE AMB :
	//per tal de poder recullir una variable dintre de la query anterior cal que cridem a la funció tipus DBO->prepare($query) que retorna un objecte
	//	de tipus statement que té els seus propis metodes per a preparar la query.
	$statement1 =  $dbh->prepare( $query );
	//fent servir mètodes de statement modifiquem la query afegint el valor del parametre que portarà la query.
	$statement1->bindValue( 'campVal', $campVal, \PDO::PARAM_STR  );
	$statement1->execute();
	//guardarem el resultat de la query dins d'una variable que contindrà un array.
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
//print_r($r);
//die();
	//Eliminem l'objecte de coneccio
	unset($mysql);
	//array_shift per a eliminar la matriu i deixar un array asociatiu normal.
	return array_shift( $r );	
}

// SELECT * FROM product BY id
function selectAllFromProduct($idValu){
	
	//Creeem un objecte de coneccions a BBDD
	$mysql = getConnection();
	//Com que les prpoietats de $mysql són privades fem servir el getDBH per accedir a la propietat $dbh (tipus PDO)
	$dbh = $mysql->getDBH();
	//guardem la query en una variable amb un parametre (:username) que recull una variable. 
	$query = 'SELECT * FROM `productes` WHERE `id` = :idValu;';
															//VARIABLE AMB :
	//per tal de poder recullir una variable dintre de la query anterior cal que cridem a la funció tipus DBO->prepare($query) que retorna un objecte
	//	de tipus statement que té els seus propis metodes per a preparar la query.
	$statement1 =  $dbh->prepare( $query );
	//fent servir mètodes de statement modifiquem la query afegint el valor del parametre que portarà la query.
	$statement1->bindValue( 'idValu', $idValu, \PDO::PARAM_STR  );
	//executa la consulta a una base de dades.
	$statement1->execute();
	//guardarem el resultat de la query dins d'una variable que contindrà un array.
	$r = $statement1->fetchAll( \PDO::FETCH_ASSOC );
	//Eliminem l'objecte de coneccio
	unset($mysql);
	//array_shift per a eliminar la matriu i deixar un array asociatiu normal.
	return array_shift( $r );	
}

function InsertNewProduct($r){
	//print_r($r);die();
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = 'INSERT INTO productes (nom, dataRevisio, estat, descripcio, id_preu, dataModificacio, usuari_modificacio)
					 VALUES 	(:nom, :dataRevisio, :estat, :descripcio, :id_preu, :dataModificacio, :usuari_modificacio)';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['alta_nom_prod'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataRevisio', $r['alta_revisio_prod'], \PDO::PARAM_STR );
	//print_r($r['alta_revisio_prod']);die();
	$statement1->bindValue( 'estat', $r['alta_estat_prod'], \PDO::PARAM_STR );
	$statement1->bindValue( 'descripcio', $r['alta_descripcio_prod'], \PDO::PARAM_STR );
	$statement1->bindValue( 'id_preu', $r['alta_tarifa_prod'], \PDO::PARAM_INT );
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name']. " " . $_SESSION[ 'user' ][ 'surname']), \PDO::PARAM_STR );
	$var = $statement1->execute();
}

function UpdaterNewProduct($r,$idProduct){
//Array del formulari.
	$mysql = getConnection();
	$dbh = $mysql->getDBH();
	$now = new \Datetime( 'now' );
	$query = '	UPDATE productes 
				SET nom = :nom , 
					dataRevisio = :dataRevisio, 
					estat = :estat,
					id_preu = :tarifa, 
					descripcio = :descripcio, 
					dataModificacio = :dataModificacio,
					usuari_modificacio = :usuari_modificacio
					WHERE id=:idProduct';
	$statement1 = $dbh->prepare( $query );
	$statement1->bindValue( 'nom', $r['nom'], \PDO::PARAM_STR );
	$statement1->bindValue( 'dataRevisio', $r['revisio'], \PDO::PARAM_STR );
	$statement1->bindValue( 'estat', $r['estat'], \PDO::PARAM_STR );
	$statement1->bindValue( 'tarifa', $r['tarifa'], \PDO::PARAM_INT );
	$statement1->bindValue( 'descripcio', $r['descripcio'], \PDO::PARAM_STR );	
	$statement1->bindValue( 'dataModificacio', $now->format( 'Y-m-d H:i:s' ), \PDO::PARAM_STR );
	$statement1->bindValue( 'usuari_modificacio', ($_SESSION[ 'user' ][ 'name'] . " " . $_SESSION[ 'user' ][ 'surname'] ), \PDO::PARAM_STR );
	$statement1->bindValue( 'idProduct', $idProduct, \PDO::PARAM_INT );
	$var = $statement1->execute();
	//afegir data modificacion
}

?>

