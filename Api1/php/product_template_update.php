<?php
	include_once( 'sql_repository.php' );
	include_once( 'sql_controller.php' );
	if(!empty($_POST['_send'])){
		//foreach per a sanejar inputs formulari
		foreach($_POST as $k=>$v){
			$k = netejaPrefixVariable($k);
			$formValues[$k] = cleanInput($v);
		}
		UpdaterNewProduct($formValues,$_GET['id']);
		return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=product&action=read', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));
	}	else if(empty( $_POST[ 'send' ])){
		if( empty( $_POST[ 'send' ])){
			$errors="";
			$r = selectAllFromProductByField($_GET['id'],"id");
			$formValues = $r;
		}
	}

?>
<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
$(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'
	});
  });
  </script>
</head>
		<form action=<?php echo sprintf("home_controller.php?destination=product&action=update&id=%d",$_GET['id']);?> method="POST">
			<div class="input">
				<label for = 'update_nom_prod'>Nom</label>
				<input id="update_nom_prod" name='update_nom_prod' required type="text" maxlength="25" value="<?php echo $formValues['nom'];?>"/>
			</div>
			<div class="input">
				<label for = 'update_revisio_prod'>Data revisió</label>
				<input id="datepicker" name='update_revisio_prod' type="date" placeholder="dd/mm/yyyy" value="<?php echo $formValues['dataRevisio'];?>"/>
			</div>	
			<div class="input">
				<label for = 'update_estat_prod'>Estat</label>
				<input id="update_estat_prod" name='update_estat_prod' type="text" maxlength="25" placeholder="disponible/llogat" value="<?php echo $formValues['estat'];?>"/>
			</div>	
			<div class="input">
				<label for = 'update_tarifa_prod'>Tarifa aplicable</label>
				<select id= 'update_tarifa_prod' name='update_tarifa_prod' >		
					<?php getPreusUpdate($formValues['id_preu']); ?>
				</select>
			</div>
			<div class="input">
				<label for = 'update_descripcio_prod'>Descripcio</label>
				<input id="update_descripcio_prod" name='update_descripcio_prod' required type="text" maxlength="100"  value="<?php echo $formValues['descripcio'];?>"></textarea>
			</div>
		<br>
		<input name="_send" type="submit" value="Modificar">
	</form>
</html>
