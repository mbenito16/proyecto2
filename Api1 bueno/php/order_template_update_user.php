<?php
	include_once( 'sql_repository.php' );
	include_once( 'sql_controller.php' );
	
	//VALIDACIO
	
	if(!empty($_POST['_send'])){
		//foreach per a sanejar inputs formulari
		foreach($_POST as $k=>$v){
			$k = netejaPrefixVariable($k);
			$formValues[$k] = cleanInput($v);
		}
		UpdaterNewOrder($formValues,$_GET['id']);
		print_r("<div class=\"alert alert-success\"><h1>Comanda modificada</h1></div>");die();
		return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=order&action=read', 
						$_SERVER['SERVER_ADDR'], getBaseURI()));
	}	else if(empty( $_POST[ 'send' ])){
		if( empty( $_POST[ 'send' ])){
			$errors="";
			$r = selectAllFromOrderByField($_GET['id'],"id");
			$formValues = $r;
//print_r($r);//*************************************************************
		}
	}

?>
		<form action=<?php echo sprintf("home_controller.php?destination=order&action=update&id=%d",$_GET['id']);?> method="POST">
			<div class="input">
					<label for = 'update_client_order'>Nom Client</label>
					<select id= 'update_client_order' name='update_client_order'>		
						<option value="<?php echo ($_SESSION['user']['id']); ?>" selected="selected" > <?php echo ($_SESSION['user']['name'] . " " . $_SESSION['user']['surname'] ); ?></option>
					</select>
			</div>
			<div class="input">
				<label for = 'update_materialRetornat_order'>Material Retornat</label>
				<select id= 'update_materialRetornat_order' name='update_materialRetornat_order' >	
						<option value="no" > Llogat </option>	
						<option value="si" > Tornat </option>
				</select>			
			</div>
			<div class="input">
				<label for = 'update_pagat_order'>Pagat</label>
				<select id= 'update_pagat_order' name='update_pagat_order' >
					<option value="no" > No pagat </option>		
					<option value="si" > Pagat </option>			
				</select>
			</div>
				<label for = 'update_esdeveniment_order'>Esdeveniments</label>
				<select id= 'update_esdeveniment_order' name='update_esdeveniment_order' >		
					<?php getEventsUpdate($formValues['id_esdeveniment']); ?>
				</select>
			</div>
			<br><br>
			<h3 class='subtitle'>Productes</h3>
			<div class="productes">
				<?php getInputsProductesUpdate(); ?>
			</div>	
		<br>
		<input name="_send" type="submit" value="Modificar">
	</form>
</div>
