<?php
	$error="";
	//si hem fet submit al formulari
	if( !empty( $_POST[ '_send' ])){
		include_once( 'sql_repository.php' );
		include_once( 'sql_controller.php' );
		//sanejem entrada
		foreach ($_POST as $k => $v){
			$formValues[$k]= cleanInput($v);			
		}

			InsertNewProduct($formValues);
			print_r("<div class=\"alert alert-success\"><h1>Producte creat</h1></div>");die();
			$r=selectAllPreus();

			return header( sprintf( 'Location: http://%s/%shome_controller.php?destination=product', 
						$_SERVER['SERVER_ADDR'], getBaseURI() ));			
	}else{
		
		//generem el contingut buit pels camps value.
		$formValues = Array ( 
			"alta_nom_prod" => "",
			"alta_revisio_prod" => "",
			"alta_estat_prod" => "",
			"alta_descripcio_prod" => "",
			"alta_tarifa_prod"=>"",
		);
		
	}
?>

<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
$(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'
	});
  });
  </script>
</head>
<form id="product_create_form" action=<?php echo sprintf("home_controller.php?destination=product&action=create");?> method="POST">
	<p><?php echo $error ?></p>
	<div class="input">
		<label for = 'alta_nom_prod'>Nom</label>
		<input id="alta_nom_prod" name='alta_nom_prod' required type="text" maxlength="25" placeholder="Introdueix aqui el nom" value="<?php echo $formValues['alta_nom_prod'];?>"/>
	</div>
	<div class="input">
		<label for = 'alta_revisio_prod'>Data revisió</label>
		<input id="datepicker" name='alta_revisio_prod' required type="date" placeholder="yyyy/mm/dd" value="<?php echo $formValues['alta_revisio_prod'];?>"/>
	</div>	
	<div class="input">
		<label for = 'alta_estat_prod'>Estat</label>
		<input id="alta_estat_prod" name='alta_estat_prod' type="text" maxlength="25" placeholder="disponible/llogat" value="<?php echo $formValues['alta_estat_prod'];?>"/>
	</div>	
	<div class="input">
		<label for = 'alta_tarifa_prod'>Tarifa aplicable</label>
		<select id= 'alta_tarifa_prod' name='alta_tarifa_prod'>		
			<?php getPreus(); ?>
		</select>
	</div>
	<div class="input">
		<label for = 'alta_descripcio_prod'>Descripció</label>
		<input id="alta_descripcio_prod" name='alta_descripcio_prod' required type="text" maxlength="100" placeholder="Introdueix aqui una petita descripcio" value="<?php echo $formValues['alta_descripcio_prod'];?>"></textarea>
	</div>
	<input name="_send" type="submit" value="REGISTRAR">
</form>
</html>
