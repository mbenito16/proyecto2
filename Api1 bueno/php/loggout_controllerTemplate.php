<?php
    include_once( 'api_utils.php' );

    sessionStart();
    logout();
    return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
		$_SERVER['SERVER_ADDR'], getBaseURI() ) );
?>
