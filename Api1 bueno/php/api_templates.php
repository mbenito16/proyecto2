<?php
    include_once( 'sql_repository.php' );
//*********
//CAPÇALERA
//*********


function getHeader(){
	garantAcces();
?>	
	<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Home</title>
        <link href="../css/index.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
	<div id="titulo">
		<p class="titol">DIVEMANAGER</p>
		<p class="usuario">Hola <?php echo $_SESSION[ 'user' ][ 'name' ] . " " . $_SESSION[ 'user' ][ 'rol' ]?></p>
		<a href="<?php echo sprintf( 'http://%s/%sloggout_controllerTemplate.php', $_SERVER['SERVER_ADDR'],
					getBaseURI() ); ?>" style="float:right; text-decoration: none;">
			Desconectar
		</a>
	</div>
	
<?php
}


//*********
//MENU DESTINATION
//*********

//funció que a partir d'un rol d'usuari mostrarà un menú amb les opcions habilitades
function mostraMenuDestination($userVal){
	$rol=$_SESSION[ 'user' ][ 'rol' ];
	$rolesAvailables = getAvailableRoles();
	echo('<div id="header">');
	echo('	<ul class="nav">');
	if(in_array('order',$rolesAvailables[$rol]['menu'])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=order">COMANDES</a>',$_SERVER['SERVER_ADDR'], getBaseURI());
	}
	if(in_array('event',$rolesAvailables[$rol]['menu'])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=event">ESDEVENIMENTS</a>',$_SERVER['SERVER_ADDR'], getBaseURI());
	}
	if(in_array('product',$rolesAvailables[$rol]['menu'])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=product">PRODUCTES</a>',$_SERVER['SERVER_ADDR'], getBaseURI() );
	}
	if(in_array('user',$rolesAvailables[$rol]['menu'])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=user">USUARIS</a>',$_SERVER['SERVER_ADDR'], getBaseURI());

	}
	if(in_array('price',$rolesAvailables[$rol]['menu'])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=price">PREUS</a>',$_SERVER['SERVER_ADDR'], getBaseURI());

	}
	echo('	</ul>');
	echo('</div>');

}



//*********
//MENU ACTION
//*********
//ACTIONS EN BARRA DE MENU
//funció que a partir d'un rol d'usuari, un nom d'usuari i una destinació mostrarà un menú amb les accions habilitades
function mostraMenuAction($destination){
	$rol=$_SESSION[ 'user' ][ 'rol' ];
	$desti="";
	switch($_GET['destination']){
		case "order": $desti= "comandes";break;
		case "event": $desti= "esdeveniments";break;
		case "product": $desti= "productes";break;
		case "user": $desti= "usuaris";break;
		case "price": $desti= "preus";break;
	}
	?>
	<div id="cos">
		<br><h2 class="title">Gestió de <?php echo $desti ?></h2><br>
	<section>
	<div>
	<?php
	$rolesAvailables = getAvailableRoles();
	echo('<div id="header">');
	echo('	<ul class="nav">');
	if(in_array('create',$rolesAvailables[$rol][$destination])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=%s&action=create">NOU</a>',$_SERVER['SERVER_ADDR'], getBaseURI(), $destination );
	}
	if(in_array('createUser',$rolesAvailables[$rol][$destination])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=%s&action=createUser">NOU</a>',$_SERVER['SERVER_ADDR'], getBaseURI(), $destination );
	}
	if(in_array('read',$rolesAvailables[$rol][$destination])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=%s&action=read">LLISTA</a>',$_SERVER['SERVER_ADDR'], getBaseURI(), $destination );

	}
	if(in_array('readUser',$rolesAvailables[$rol][$destination])){
		echo sprintf('<a href="http://%s/%shome_controller.php?destination=%s&action=readUser">LLISTA</a>',$_SERVER['SERVER_ADDR'], getBaseURI(), $destination );

	}
	echo('	</ul>');
	echo('</div>');
}

//ACTIONS EN TAULES
//funcio que inclou els botons per la action READ a cada camp la taula en funcio del rol i el destination on estem
function getBotonsReadTable($v,$destination){
	$rolesAvailables = getAvailableRoles();
	$rol=$_SESSION[ 'user' ][ 'rol' ];
//update
	if(in_array('update',$rolesAvailables[$rol][$destination])){
		echo '<td>';
?>	
		<a href="<?php echo sprintf( 'http://%s/%shome_controller.php?destination=%s&action=update&id=%d',
		$_SERVER['SERVER_ADDR'], getBaseURI(), $destination, $v['id'] ); ?>">Modifica</a>
<?php
		echo '</td>';
	}
//updateUser	
	if(in_array('updateUser',$rolesAvailables[$rol][$destination])){
		echo '<td>';
?>	
		<a href="<?php echo sprintf( 'http://%s/%shome_controller.php?destination=%s&action=updateUser&id=%d',
		$_SERVER['SERVER_ADDR'], getBaseURI(), $destination, $v['id'] ); ?>">Modifica</a>
<?php
		echo '</td>';
	}
//sign	
	if(in_array('sign',$rolesAvailables[$rol][$destination])){
		echo '<td>';
?>
		<a href="<?php echo sprintf( 'http://%s/%shome_controller.php?destination=%s&action=sign&id=%d',
		$_SERVER['SERVER_ADDR'], getBaseURI(), $destination, $v['id'] ); ?>">Apuntar-se</a>
<?php
		echo '</td>';
	}
//delete
	if(in_array('delete',$rolesAvailables[$rol][$destination])){
		echo '<td>';
?>
		<a href="<?php echo sprintf( 'http://%s/%shome_controller.php?destination=%s&action=delete&id=%d',
		$_SERVER['SERVER_ADDR'], getBaseURI(), $destination, $v['id'] ); ?>">Eliminar</a>
<?php
		echo '</td>';
	}
}

//funcio que a partir d'un rol d'usuari, un usuari, una destinació i una acció carrega la vista.
function mostraViewAction($destination, $action){	
	$rol=$_SESSION[ 'user' ][ 'rol' ];
	switch($_GET['destination']){
		case 'user': if(isRoleAllowedForAction($destination, $action )){
						include_once 'user_controller.php';
					 }else{
						return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
						$_SERVER['SERVER_ADDR'], getBaseURI() ));
					 }
					 break;
		case 'product': include_once 'product_controller.php';
					 break;
		case 'event': include_once 'events_controller.php';
					 break;
		case 'order': include_once 'order_controller.php';
					 break;
		case 'price': include_once 'price_controller.php';
					 break;
		default: return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
					$_SERVER['SERVER_ADDR'], getBaseURI() ));
					break;
	}
?>
        </div>
	</section>
	</div>
<?php
}

//*********
//FOOTER
//*********

function getFooter(){
?>
<footer>
		<div id="footer">
			<p class="peu">Copyright</p>
		</div>
	</footer>
	</body>
</html>
<?php
}

//*********
//BOTONS controlats per rol
//*********



//*********
//HTML SQL
//*********
//Mostra en html totes les tarifes exixtents per a un desplegable
function getPreus(){
	$r=selectAllPreus();	
	foreach ($r as $k=>$v){
?>	
	<option value="<?php echo ($v['id']); ?>"><?php echo ($v['nom'] . " " . $v['info']); ?></option>
<?php
	}
}

//Mostra en html totes les tarifes exixtents per a un desplegable amb el valor de la BDD asignat per defecte
function getPreusUpdate($idPreuBDD){
	$r=selectAllPreus();
	foreach ($r as $k=>$v){
?>	
	<option value="<?php echo ($v['id']); ?>" <?php echo $idPreuBDD==$v['id']? '" selected="selected"':''; ?> > <?php echo ($v['nom'] . " " . $v['info']); ?></option>
<?php
	}
}

//Mostra en html totes les usuaris existents per a un desplegable
function getUsers(){
	$r=selectAllUsers();	
	foreach ($r as $k=>$v){
?>	
	<option value="<?php echo ($v['id']); ?>"><?php echo ($v['nom'] . " " . $v['cognom']); ?></option>
<?php
	}
}

//Mostra en html totes les usuaris existents per a un desplegable amb el valor de la BDD asignat per defecte
function getUserUpdate($idUserBDD){
	$r=selectAllUsers();
	foreach ($r as $k=>$v){
?>	
	<option value="<?php echo ($v['id']); ?>" <?php echo $idUserBDD==$v['id']? '" selected="selected"':''; ?> > <?php echo ($v['nom'] . " amb el DNI " . $v['DNI']); ?></option>
<?php
	}
}

//Mostra en html totes les tarifes exixtents per a un desplegable amb el valor de la BDD asignat per defecte
function getRolUpdate($idRolBDD){
	$r=getRols();
	foreach ($r as $k){
?>	
	<option value="<?php echo ($k); ?>" <?php echo $idRolBDD==$k? '" selected="selected"':''; ?> > <?php echo ($k); ?></option>
<?php
	}
}

//Mostra en html tots els esdeveniment existents per a un desplegable
function getEvents(){
	$r=selectAllEvents();	
	foreach ($r as $k=>$v){
?>	
	<option value="<?php echo ($v['id']); ?>"><?php echo ($v['nom'] . " -> " . $v['dataInici']); ?></option>
<?php
	}
}

//Mostra en html el esdeveniment seleccionat per a un desplegable
function getEventSelected($idEsdeveniment){
	$r=selectAllFromEventsByField($idEsdeveniment, 'id');
?>	
	<option value="<?php echo ($idEsdeveniment); ?>"><?php echo ($r['nom'] . " -> " . $r['dataInici']); ?></option>
<?php
}

//MOstra tots o el esdeveniment selecciona en cas de que Existeix $GET[id]
function getEventsOrSelected(){
	if(isset($_GET['id'])){
		getEventSelected($_GET['id']);
	}else{
		getEvents();
	}
}

//Mostra en html tots els esdeveniment existents per a un desplegable amb el valor de la BDD asignat per defecte
function getEventsUpdate($idEventBDD){
	$r=selectAllEvents();
	foreach ($r as $k=>$v){
?>	
	<option value="<?php echo ($v['id']); ?>" <?php echo $idEventBDD==$v['id']? '" selected="selected"':''; ?> > <?php echo ($v['nom'] . " -> " . $v['dataInici']); ?></option>
<?php
	}
}

//afegeix el numero de productes disponibles al select de productes d'una comanda
function getNumProductes($numeroProductes){
	for($i=0;$i<$numeroProductes;$i++){
		?>	
			<option value="<?php echo $i; ?>"><?php  echo $i; ?></option>
		<?php	
	}
}

//afegeix el numero de productes disponibles al select de productes d'una comanda amb el valor de la BDD preseleccionat
function getNumProductesUpdate($numeroProductes, $numeroProductesBDD){
	for($i=0;$i<$numeroProductes;$i++){
		?>	
			<option value="<?php echo $i; ?>" <?php echo $numeroProductesBDD==$i? '" selected="selected"':''; ?> ><?php  echo $i; ?></option>
		<?php	
	}
}


//Recull tots els productes de la BBDD i els retorna en formulari per afegir a la comanda
function getInputsProductes(){
	$r=selectAllPreus();
	foreach ($r as $k=>$v){
?>
	<div class="input">
		<label for = "<?php echo sprintf('productes_order_%s',$v['id']);?>"><?php echo $v['nom'];?></label>
		<select id= "<?php echo sprintf('productes_order_%s',$v['id']);?>" name="<?php echo sprintf('productes_order_%s',$v['id']);?>">		
			<?php getNumProductes(5); ?>
		</select>
	</div>	
<?php
	}
}

//Recull tots els productes de la BBDD i els retorna en formulari per afegir a la comanda amb el valor que tenien
function getInputsProductesUpdate(){
	$p = selectAllPreus();
	$r =selectFromProductesReservatsByField($_GET['id'],'id_comanda');
	foreach ($p as $k=>$v){
		$s=selectAllFromPriceByField($r[$v['id']-1]['id_preuProducte'], 'id');;
		$numeroProductesBDD = $r[$v['id']-1]['numero_productes']; 
?>
		<div class="input">
			<label for = "<?php echo sprintf('productes_order_%s',$r[$v['id']-1]['id_preuProducte']);?>"><?php echo $v['nom']?></label>
			<select id= "<?php echo sprintf('productes_order_%s',$r[$v['id']-1]['id_preuProducte']);?>" name="<?php echo sprintf('productes_order_%s',$r[$v['id']-1]['id_preuProducte'])?>">
				<?php getNumProductesUpdate(4, $numeroProductesBDD); ?>
			</select>
		</div>	
<?php
	}
}


?>
