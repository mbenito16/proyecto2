CREATE DATABASE blanes_sub_test;

DROP TABLE participacio;
DROP TABLE esdeveniments;
DROP TABLE usuaris;
DROP TABLE preus;
DROP TABLE productes;
DROP TABLE productesReservats;
DROP TABLE comandes;

CREATE TABLE usuaris (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(25) NOT NULL,
cognom VARCHAR(100),
email VARCHAR(100) NOT NULL UNIQUE,
rolUsuari VARCHAR(15) NOT NULL,
DNI VARCHAR(15) NOT NULL UNIQUE,
pass VARCHAR(100) NOT NULL,
salt VARCHAR(255) NOT NULL,
titulacio VARCHAR(100),
nivellTitulacio VARCHAR(100),
numeroTitulacio VARCHAR(100),
adreca VARCHAR(100),
telefon VARCHAR(15), 
asseguranca VARCHAR(100),
polissa VARCHAR(50),
caducitat DATE,
tarifaClient VARCHAR(1),
punts INT,
dataModificacio DATE,
usuari_modificacio VARCHAR(130) NOT NULL
);

CREATE TABLE preus(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(100) NOT NULL,
info VARCHAR(100) NOT NULL,
preu_a INT,
preu_b INT,
preu_c INT,
preu_d INT,
dataModificacio DATE,
usuari_modificacio VARCHAR(130) NOT NULL
);

CREATE TABLE productes(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(25) NOT NULL, 
dataRevisio DATE NOT NULL,
estat VARCHAR(25) NOT NULL,
descripcio VARCHAR(100),
dataModificacio DATE,
usuari_modificacio INT,
id_preu int,
FOREIGN KEY (id_preu) REFERENCES preus(id)
);


CREATE TABLE esdeveniments(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(25) NOT NULL,
lloc_sortida VARCHAR(25) NOT NULL,
dataInici DATE NOT NULL,
dataModificacio DATE,
usuari_modificacio INT
);

CREATE TABLE comandes(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
materialRetornat VARCHAR(10) NOT NULL,
comandaPagat VARCHAR(20),
id_client INT,
id_esdeveniment INT, 
preu DEC(5,2),
dataModificacio DATE,
usuari_modificacio VARCHAR(130) NOT NULL,
FOREIGN KEY (id_client) REFERENCES usuaris(id),
FOREIGN KEY (id_esdeveniment) REFERENCES esdeveniments(id)
);

CREATE TABLE participacio(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
id_esde INT,
id_usua INT,
FOREIGN KEY (id_esde) REFERENCES esdeveniments(id),
FOREIGN KEY (id_usua) REFERENCES usuaris(id)
);


CREATE TABLE productesReservats(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
id_preuProducte INT,
id_comanda INT,
numero_productes INT,
FOREIGN KEY (id_preuProducte) REFERENCES preus(id),
FOREIGN KEY (id_comanda) REFERENCES comandes(id)
);
