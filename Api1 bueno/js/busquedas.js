
//////////////FUNCIONES///////////////////////FUNCIONES/////////////////////////////////////////////////////////////////
//JQUERY
function dialogo(){
	$("#dialogo").dialog("open");
};

function dialogo2(){
	$("#dialogo2").dialog("open");
};

function PasarDatosPhp(){
codi_pedido=codi_pedido+1;
	if(window.XMLHttpRequest) {
    xhttp = new XMLHttpRequest();
  }
  else if(window.ActiveXObject) {
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhttp.open("POST", "php/insert.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.onreadystatechange = function() {
  if (xhttp.readyState == 4 && xhttp.status == 200) {
    //document.getElementById("register").innerHTML = xhttp.responseText;
  }
  };
  
  
  observaciones=document.getElementById("observaciones").value;
  f_estimada=document.getElementById("datepicker").value;
  var param="fecha_estimada="+f_estimada+"&observaciones="+observaciones+"&codi_pedido="+codi_pedido+"&estado="+estado+"&f_pedido="+hoy+"&codi_cliente="+codi_cliente;
  alert(param);
  xhttp.send(param);
	
}

//CALENDARIO JQUERY UI
jQuery(function($){
	$.datepicker.regional['es'] = {
		maxDate: "+5D",
		minDate: "0D",
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
}); 

//CALCULAR IMPORTE
function calcularImporte(){
	$('.precios').each (function(){
		array_precios[array_precios.length]=this.value;	
	});
	$('.cantidades').each (function(){
		array_cantidades[array_cantidades.length]=this.value;	
	});
	for(i=0;i<array_cantidades.length;i++){
		imp_total=((array_cantidades[i]*array_precios[i])+(imp_total));
	}
	$('#imp_total').text("Importe total: "+imp_total.toFixed(2));
	imp=imp_total;
	imp_total=0;
	array_cantidades=[];
	array_precios=[];
}
//AGREGAR LINEAS
function agregarLineas(){
	cont=cont+1;
	$("#tproductos").append('<tr id="linea'+cont+'"><td class="lineas" id="l'+cont+'">'+cont+'</td><td>'+pasar_prod.producto+'</td><td>'+pasar_prod.proveedor+'</td><td>'+pasar_prod.stock+'</td><td>'+pasar_prod.precioventa+'<td>'+pasar_prod.precioproveedor+'</td><td><input id="c'+cont+'" class="cantidades" type=\"number\" min=\"1\" max=\"'+pasar_prod.stock+'\" value="1" onchange=\"recogerValorCantidad(this.id,this.value);calcularImporte();comprobarSaldos();\" onKeyPress="return false" > </td><td><input class="precios" onKeyPress="return false" step="0.1" onchange=\"calcularImporte();comprobarSaldos();\" type=\"number\" min=\"'+pasar_prod.precioproveedor+'\" max=\"'+pasar_prod.precioventa+'\" value=\"'+pasar_prod.precioventa+'\"></td><td><input type=\"image\" src=\"img/papelera.png\" class="imagenes" id="'+cont+'" onclick="borrarLinea(this.id)" ></td></tr>');
}

//BORRAR LINEAS
function borrarLinea(idd){
	$('#linea'+idd).remove();
	calcularImporte();
}

//RECOGER VALOR CANTIDAD
function recogerValorCantidad(vc,vv){
	tmpcantidad=vc;
	tmpvalor=vv;
	
}

//RECOGER VALOR FECHA
function recogerValorFecha(vf){
	f_esperada=vf;
}

//RECOGER COMENTARIOS
function recogerComentarios(vco){
	coment=vco;
}

//PINTAR VALOR CUANDO SOBREPASA EL LIMITE
function pintarValor(){
	tmpvalor=tmpvalor-1;
	$('#'+tmpcantidad).val(tmpvalor);	
	calcularImporte();
	
}

//COMPRUEBA QUE HAYA ELEGIDO UN CLIENTE Y TAMBIEN COMPRUEBA QUE EL IMPORTE TOTAL SEA
//MAS GRANDE QUE EL SALDO DEL CLIENTE
function comprobarSaldos(){
	if(limite==0){
		error=1;
		alert("Has de elegir un cliente");
	}else if (imp >= limite){
		error=2;
		alert("Has sobrepasado el importe del cliente ");
		pintarValor();
	} else if (imp < limite){
		$('#ok').attr("disabled", false);
		$('#agregar').attr("disabled", false);
		error=0;
	}
}

//FUNCION AL DARLE AL BOTON DE ACEPTAR(REALIZAR COMANDA) en este boton
// namas deberia permitir en intert o no
function calcularSaldoCliente(){
	codi_pedido=codi_pedido+1;
	if(limite == 0){
		alert("has de escojer un cliente");
	}

	if(imp == 0){
		alert("has de escojer al menos un producto");
	}
	
	if(imp>=limite){
		alert("No se guardaran los datos del pedido. ");
	}else{
		//alert("pues nda");
	}
	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//WINDOW ONLOAD
window.onload=function(){
	var dialogOpts = {
		width: 1000,
		height: 580,
		autoOpen: false,
		buttons: {
			"Aceptar": function () {
				$(this).dialog("close");
				
			}
		}
	};
	
	var dialogOpts2 = {
		width: 1000,
		height: 580,
		autoOpen: false,
		buttons: {
			"Aceptar": function () {
				calcularImporte();
				comprobarSaldos();
				if(error==0){
					agregarLineas();
					calcularImporte();
				}
				$(this).dialog("close");
				
			}
		}
	};
	
	//calendario
	 $("#datepicker").datepicker(
		{
         beforeShowDay: function (day) { 
           var day = day.getDay(); 
           if (day == 0) { 
             return [false, "somecssclass"] 
           } else { 
             return [true, "someothercssclass"] 
           } 
         }           
        }
	 );
	 
	//dialogo datos clientes
	$("#dialogo").dialog(dialogOpts);
	var x=document.getElementById("buscar");
	x.onclick=dialogo;
	
	//dialogo detalles pedidos
	$("#dialogo2").dialog(dialogOpts2);
	var y=document.getElementById("agregar");
	y.onclick=dialogo2;
	
	//var ok=document.getElementById("ok");
	//ok.onclick=calcularSaldoCliente;
	
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////


//ANGULAR
//modulo cliente
var unModulo=angular.module('myapp',[]);
unModulo.controller('uncontrolador', ['$scope','$http',function($scope,$http){
	//peticion a clientes
	$http({
		method : "GET",
		url : "php/acceso.php"
	}).then(function(response) {
		$scope.Esdeveniments = response.data;
	});
	
	//peticion a productos
	$http({
		method : "GET",
		url : "php/acceso.php"
	}).then(function(response) {
		$scope.Productos = response.data;
	});
	
	$scope.datosEsdeveniments=function(e){
			$("#nom_es").text("Nom: "+e.nombre);
			$("#lloc").text("Nombre Contacto: "+e.lloc);
			$("#data").text("Data Inici: "+e.data);
			codi_cliente=e.codi_cliente;
			
	}
	
	
	

}]);
