
<?php
	$error="";
	//si hem fet submit al formulari
	if( !empty( $_POST[ '_send' ])){
		include_once( 'sql_repository.php' );
		include_once( 'sql_controller.php' );
		//sanejem entrada
		foreach ($_POST as $k => $v){
			$formValues[$k]= cleanInput($v);			
		}
		if(evalInsertNewUserUniques($formValues)){
			InsertNewUser($formValues);
			return header( sprintf( 'Location: http://%s/%sloggin_controllerTemplate.php', 
						$_SERVER['SERVER_ADDR'], getBaseURI(),  $_GET['user'] ));			
		}else{
		//generem missatge error
		$error = "Ja existeix un usuari amb aquestes credencials.";
		}	
	}else{
		
		//generem el contingut buit pels camps value.
		$formValues = Array ( 
			"alta_nom" => "",
			"alta_cognom" => "",
			"alta_DNI" => "",
			"alta_email" => "",
			"alta_pass" => "", 
			"alta_titulacio" => "",
			"alta_nivellTitulacio" => "",
			"alta_numeroTitulacio" => "",
			"alta_adreca" => "",
			"alta_telefon" => "",
			"alta_asseguranca" => "",
			"alta_polissa" => "",
			"alta_caducitat" => ""
		);
		
	}
?>

<!DOCTYPE html>
<head>
<link href="../css/index.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<div id="titulo">
		<p class="titol">DIVEMANAGER</p>
		<div class="boto">
<!--			<input type="button" value="Access" href="#">-->
		</div>
	</div>
	<div id="cos">
		<form action="registre_controllerTemplate.php" method="POST">
		<p><?php echo $error ?></p>
		<div class="input">
			<label for = 'alta_nom'>nom</label>
			<input id="alta_nom" name='alta_nom' required type="text" maxlength="25" value="<?php echo $formValues['alta_nom'];?>"/>
		</div>
		<div class="input">
			<label for ='alta_cognom'>cognom</label>
			<input id="alta_cognom" name='alta_cognom' required type="text" maxlength="100" value="<?php echo $formValues['alta_cognom'];?>"/>
		</div>
		<div class="input">
			<label for = 'alta_DNI'>DNI</label>
			<input id="alta_DNI" name='alta_DNI' required type="dni" maxlength="12" value="<?php echo $formValues['alta_DNI'];?>"/>
		</div>
		<div class="input">
			<label for = 'alta_pass'>password</label>
			<input id="alta_pass" name='alta_pass'  required type="password" maxlength="100" value="<?php echo $formValues['alta_pass'];?>"/>
		</div>	
		<div class="input">
			<label for = 'alta_email'>email</label>
			<input id="alta_email" name='alta_email' required type="email" maxlength="100"  value="<?php echo $formValues['alta_email'];?>"/>
		</div>	
		<div class="input">
			<label for = 'alta_titulacio'>titulacio</label>
			<input id="alta_titulacio" name='alta_titulacio' type="text" maxlength="100" value="<?php echo $formValues['alta_titulacio'];?>"/>
		</div>	
		<div class="input">
			<label for = 'alta_nivellTitulacio'>nivell de titulacio</label>
			<input id="alta_nivellTitulacio" name='alta_nivellTitulacio' type="text" maxlength="100" value="<?php echo $formValues['alta_nivellTitulacio'];?>"/>
		</div>	
		<div class="input">
			<label for = 'alta_numeroTitulacio'>numero de titulacio</label>
			<input id="alta_numeroTitulacio" name='alta_numeroTitulacio' type="text" maxlength="100" value="<?php echo $formValues['alta_numeroTitulacio'];?>"/>
		</div>	
		<div class="input">
			<label for = 'alta_adreca'>adreca</label>
			<input id="alta_adreca" name='alta_adreca' type="text" maxlength="100" value="<?php echo $formValues['alta_adreca'];?>"/>
		</div>
		<div class="input">
			<label for = 'alta_telefon'>telefon</label>
			<input id="alta_telefon" name='alta_telefon' type="tel" maxlength="15" value="<?php echo $formValues['alta_telefon'];?>"/>
		</div>
		<div class="input">
			<label for = 'alta_asseguranca'>asseguranca</label>
			<input id="alta_" name='alta_asseguranca' type="text" maxlength="100" value="<?php echo $formValues['alta_asseguranca'];?>"/>
		</div>
		<div class="input">
			<label for = 'alta_polissa'>polissa</label>
			<input id="alta_polissa" name='alta_polissa' type="text" maxlength="50" value="<?php echo $formValues['alta_polissa'];?>"/>
		</div>
		<div class="input">
			<label for = 'alta_caducitat'>caducitat</label>
			<input id="alta_caducitat" name='alta_caducitat' type="date" value="<?php echo $formValues['alta_caducitat'];?>"/>
		</div>
		<input name="_send" type="submit" value="REGISTRAR">
	</div>
	</div>
	<!--<div id="footer">
		<p class="peu">Copiryght</p>
	</div>-->
</body>
</html>
